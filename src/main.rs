
use std::collections::HashSet;
use std::mem;

use rand::seq::index;

#[derive(Default,Debug)]
struct Universe {
    currentgen: HashSet<(i32,i32)>,
    prevgen: HashSet<(i32,i32)>,
    envelope: HashSet<(i32,i32)>,
    changes: HashSet<(i32,i32)>,
    scratch: HashSet<(i32,i32)>,
    births: u32,
    survival: u32,
    cells_edited: bool
}

impl Clone for Universe {
    fn clone(&self) -> Self {
        Universe {
            currentgen: self.currentgen.clone(),
            prevgen: self.prevgen.clone(),
            envelope: self.envelope.clone(),
            changes: self.changes.clone(),
            scratch: HashSet::new(),
            births: self.births,
            survival: self.survival,
            cells_edited: self.cells_edited,
        }
    }
}

fn neighborhood(cell: (i32, i32)) -> impl Iterator<Item=(i32,i32)> {
    (-1..=1).flat_map(move |dx| (-1..=1).map(move |dy| (cell.0+dx, cell.1+dy)))
}

impl Universe {
    fn advance(&mut self) {
        self.scratch.clear();
        self.scratch.extend(
            self.changes
            .iter()
            .flat_map(|cell| neighborhood(*cell))
        );

        //If the cells have been edited then the prevgen is not consistent
        //with the life rules. In this case we can add to the changes set,
        //but it is not safe to remove changes.
        if !self.cells_edited {
            self.changes.clear();
        };

        for cell in self.scratch.iter() {
            let livecells = neighborhood(*cell)
                .filter(|neighbor| self.currentgen.contains(neighbor))
                .count();
            let livecell = 
                if self.currentgen.contains(cell) {
                    (self.survival >> (livecells - 1)) & 1 != 0
                } else {
                    (self.births >> livecells) & 1 != 0
                };
            if livecell {
                if !self.prevgen.contains(cell) {
                    self.prevgen.insert(*cell);
                    self.changes.insert(*cell);
                    self.envelope.insert(*cell);
                };
            } else if self.prevgen.contains(cell) {
                    self.prevgen.remove(cell);
                    self.changes.insert(*cell);
            };
        }
        mem::swap(&mut self.currentgen, &mut self.prevgen);
        self.cells_edited = false;
    }

    fn insert(&mut self, cell: (i32, i32)) {
        self.currentgen.insert(cell);
        self.envelope.insert(cell);
        self.changes.insert(cell);

        // the first advance after changes needs to be handled specially.
        self.cells_edited = true;
    }
}



struct Glider<'a>{
    lane: i32,
    rows: i32,
    previous: Option<&'a Glider<'a>>,
}

fn shoot_gliders(
    start: &Universe,
    gliderrows_left: i32,
    gliders: Option<&Glider>,
) -> bool {
    let mut rng = rand::thread_rng();
    let minlane = start.currentgen.iter().map(|(x,y)| x-y).min().unwrap();
    let maxlane = start.currentgen.iter().map(|(x,y)| x-y).max().unwrap();
    let startingline = start.currentgen.iter().map(|(x,y)| x+y).max().unwrap();

    for lane in index::sample(
        &mut rng,
        (maxlane + 9 - minlane) as usize,
        5 as usize,
    ).iter().map(|l| l as i32 - minlane - 4) {
        let mut universe = start.clone();

        let x = 4 + startingline/2  + lane/2;
        let y = 4 + startingline/2  - (lane - lane/2);

        //insert glider
        universe.insert((x,y));
        universe.insert((x,y+1));
        universe.insert((x,y+2));
        universe.insert((x+1,y));
        universe.insert((x+2,y+1));

        let mut gliderrows = 0;
        //stabilize universe
        for rows in 0..200 {
            universe.advance();
            universe.advance();
            if universe.changes.is_empty() {
                gliderrows = rows + 20;
                break;
            };
        };

        if gliderrows == 0 || universe.currentgen.is_empty() {
            continue;
        };

        let glider = Glider {lane, rows: gliderrows, previous: gliders};

        // Check if we've produced a block.
        let (x,y) = universe.currentgen.iter()
            .min_by(|(x1,y1),(x2,y2)| (x1+y1).cmp(&(x2+y2))).unwrap();

        if universe.currentgen.contains(&(x+1,*y))
            && universe.currentgen.contains(&(*x,y+1))
            && universe.currentgen.contains(&(x+1,y+1))
        {
            let envelope = &universe.envelope;
            if !envelope.contains(&(x-1,*y))
                && !envelope.contains(&(x-1,y+1))
                && !envelope.contains(&(x-1,y+2))
                && !envelope.contains(&(x-1,y+3))
                && !envelope.contains(&(x-1,y+4))
                && !envelope.contains(&(x-1,y+5))
                && !envelope.contains(&(x-2,y+6))
                && !envelope.contains(&(*x,y-1))
                && !envelope.contains(&(x+1,y-1))
                && !envelope.contains(&(x+2,y-1))
                && !envelope.contains(&(x+3,y-1))
                && !envelope.contains(&(x+4,y-1))
                && !envelope.contains(&(x+5,y-1))
                && !envelope.contains(&(x+6,y-2))
            {

                //print out the solution
                println!("x = 600, y = 600, rule = B3/S23");
                println!("2o$2o$8$");
                print_gliders(Some(&glider));
                println!("!");
                return true;
            };

            if universe.currentgen.len() == 4 {
                return false; // don't bother recursing for a block
            }
        };

        if gliderrows + 3 < gliderrows_left {
            //recursively call to add another glider
            if shoot_gliders(
                &universe,
                gliderrows_left - gliderrows - 3,
                Some(&glider),
            ) {
                return true;
            }
        };
    };
    false
}

fn print_gliders(glider: Option<&Glider>) -> i32 {
    match glider {
        None => 10,
        Some(g) => {
            let row = print_gliders(g.previous);
            println!(
                "{}b2o${}bobo${}bo{}$",
                row + g.lane,
                row + g.lane,
                row + g.lane,
                g.rows + 1,
            );
            row + g.rows + 3
        }
    }
}


fn main() {

    let mut universe = Universe::default();
    universe.births = 0b0000_1000;
    universe.survival = 0b0000_1100;

    // Start with a block for a target
    universe.insert((0,0));
    universe.insert((0,1));
    universe.insert((1,0));
    universe.insert((1,1));

    loop {
        shoot_gliders(&universe,500,None);
    };
}
