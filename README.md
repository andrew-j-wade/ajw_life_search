This is a program that found this construction:

<https://www.conwaylife.com/forums/viewtopic.php?f=2&t=4020#p90099>

## Usage

1. Install <https://www.rust-lang.org>

2. Clone the repository.

3. Run "cargo build --release".

4. Run the executable in the target/release directory.
 
If a construction is found it will be output to the console. This may take a
very long time.

The search order is random and the program is single-threaded so running
multiple copies at once makes sense.
